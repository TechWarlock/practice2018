#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <string.h>

void readfile(FILE *database, char *query); //������� ������ � ������ ���������� �� �����; ���������� ���� �� � ������ ����������������� ������� ��� ���������
void savetofile(FILE *database, char *query); //������� ������ � ������ ���������� �� ����� � ����������� ����������� ������ � ����, ��� ������������� � ������������; ���������� ���� �� � ������ ����������������� ������� ��� ���������
void filter(char *string); //������� ������� ������ �� ������������ ��� ����������� ������ �� �����, ���������� ������ � �������� ���������
void userchoice(FILE *database, char *query); //������� ����������������� ������, ��������� ������� ������ ��� ������ � ����������� ��� ������� ������ � ������ ������������� �����, ��������� ���� �� � ������ ����������������� ������� ��� �������� ������ ��������
char searchquery(char *search); //������� ������� ����������������� ������, ��������� ������ ��� ������ � �������� ���������

int main(void)
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	char usersearch[50];
	FILE *database = fopen("perelik_vnz_data.csv", "r");
	searchquery(usersearch);
	userchoice(database, usersearch);
	int fclose(FILE *database);
	system("pause");
	return 0;
}

void userchoice(FILE *database, char *query)
{
	int user=0;
	printf("C�������� ���������� ������ � ����?\n1 - ��\n2 - ���\n");
	while (user != 1 && user != 2)
	{
		scanf("%i", &user);
		if (user == 1 || user == 2)
		{
			if (user == 1) savetofile(database, query);
			if (user == 2) readfile(database, query);
		}
		else printf("������������ ����. �������� ����� ����.\n");
	}
	
	
}

char searchquery(char *search)
{
	printf("������� ��������� ������: ");
	scanf("%s", search);
	return *search;
}

void filter(char *string)
{
	char toberemoved = ';', toreplace = ' ';
	int i = 0;
	while (string[i] != '\0')
	{
		if (string[i] == toberemoved)
			string[i] = toreplace;
		i++;
	}
}

void readfile(FILE *database, char *query)
{
	char string[500];
	while (!(feof(database)))
	{
		fgets(string, sizeof(string), database);
		if ((strstr(string, query)) != NULL)
		{
			filter(string);
			puts(string);
		}
	}
}

void savetofile(FILE *database, char *query)
{
	char fname[30];
	char string[500];
	printf("������� �������� ����� � ����������� .csv: ");
	scanf("%s", fname);
	FILE *userdata = fopen(fname, "w");
	while (!(feof(database)))
	{
		fgets(string, sizeof(string), database);
		if ((strstr(string, query)) != NULL)
		{
			fputs(string, userdata);
			filter(string);
			puts(string);
		}
	}
}